#!/home/hoolio/shade/bin/python

print "Importing python modules"
from shade import *
import string
import random

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

#simple_logging(debug=True)
print "Connecting to nectar cloud"
conn = openstack_cloud(cloud='nectar')

name = "Shade testing " + id_generator(3, "6793YUIO")
image = '0f72065c-2937-4ece-b3d5-424808ca7cdf' # 16.04
flavor = '639b8b2a-a5a6-4aa2-8592-ca765ee7af63' # m2.small
security_groups = ['d971d503-00b2-4cc4-ae4a-eab91154a7d0'] # SSH
availability_zone = 'NCI'
key_name = 'hoolio'

print "Creating instance (wait a sec)"

testing_instance = conn.create_server(wait=True, auto_ip=True,
    name=name,
    image=image,
    flavor=flavor,
    security_groups=security_groups,
    key_name=key_name,
    availability_zone=availability_zone)

print " .. " + testing_instance.id
print " .. " + testing_instance.name
print " .. " + testing_instance.accessIPv4

print "Deleting instance (wait a sec)"
conn.delete_server(name_or_id=testing_instance.id,wait=True)

## bits bin
#
#images = conn.list_images()
#for image in images:
#    print(image)

# 16.04
# image_id = '0f72065c-2937-4ece-b3d5-424808ca7cdf'
# image = conn.get_image(image_id)
# print "Image is ",image.id

#flavors = conn.list_flavors()
#for flavor in flavors:
#    print(flavor)
#parsed = json.loads(flavors)

# 
#flavor_id = '639b8b2a-a5a6-4aa2-8592-ca765ee7af63'
#flavor = conn.get_flavor(flavor_id)
#print(flavor)

